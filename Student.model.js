var mongoose=require('mongoose');
var Schema = mongoose.Schema;

var StudentSchema = new Schema({
    name:String,
    school:String,
    category:String
});

module.exports = mongoose.model('Student',StudentSchema);